from faker import Faker
import numpy as np
import datetime
# TODO: multiple languages
fake = Faker('cs_CZ')

def calculateAmount(mean, std_deviation):
    return int(np.random.normal(mean, std_deviation, 1)[0])

def calculateDateTime(datetime_start=None, datetime_end=None, mean=0, std_deviation=0):
    """Calculates date based distribution in configuration file"""
    # TODO: distributions
    # return np.random.normal(mean, std_deviation, 1)[0]
    return fake.date_time_between_dates(datetime_start, datetime_end)

def getName():
    """ Generates random male or female name
    not using fake.name because it produces name sufixes/prefixes (titles)"""

    if np.random.random() < 0.5:
        return "{} {}".format(fake.first_name_female(), fake.last_name_female())
    else:
        return "{} {}".format(fake.first_name_male(), fake.last_name_male())

# TODO: make abstract Generator class and inherit others from it

class GenerateOnePaymentOrder():
    def __init__(self):
        self.payment = GeneratePayment()

class GeneratePayment():
    def __init__(self):

        self.transactionType = "WIRE_TRANSFER"
        self.amount = calculateAmount(10000, 1000)
        self.currency = fake.currency_code()
        self.submissionDate = calculateDateTime(datetime.datetime.now()).strftime('%Y-%m-%d.%H.%M.%S')
        # self.dueDate = calculateDateTime(self.submissionDate).strftime('%Y-%m-%d.%H.%M.%S')
        # TODO: dueDate should be >= submissionDate
        self.dueDate = calculateDateTime(datetime.datetime.now()).strftime('%Y-%m-%d.%H.%M.%S')
        self.ks = str(fake.random_int(min=1, max=10000000000))
        self.ss = str(fake.random_int(min=1, max=9999))

        self.urgent = np.random.random() < 0.5  # true or false
        self.vs = str(fake.random_int(min=1, max=10000000000))
        self.additionalInformation = ""

        self.creditParty = GenerateParty()
        self.debitParty = GenerateParty()


class GenerateParty():
    def __init__(self):

        self.name = getName()
        self.address = GenerateAddress()
        self.account = GenerateAccount()

class GenerateAddress():
    def __init__(self):

        self.buildingNumber = str(fake.random_int(min=1, max=9999))
        self.city = fake.city()
        #self.country = fake.country() # TODO: country based on locale
        self.country = "Česká Republika" # use only CR for now because of czech cities
        self.registryBuildingNumber = str(fake.random_int(min=1, max=9999))
        self.zipCode = fake.postcode()
        self.addressLine1 = "{} {}".format(fake.street_name(), self.buildingNumber)
        self.addressLine2 = "{} {}".format(self.zipCode, self.city)
        self.street = "{} {}".format(self.addressLine1, self.addressLine2)
        self.cityCombined = "{} {}".format(self.city, self.zipCode)

class GenerateAccount():
    def __init__(self):

        self.iban = fake.iban()
