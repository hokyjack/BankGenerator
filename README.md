# Bank Generator

This Python library provides an easy way to generate
random imitations of bank transactions, constrained by a supplied configuration file.

The library uses [Faker library](https://github.com/joke2k/faker) for generating
random data such as names, addresses, bank numbers or dates.

## Description

This generated data are then grouped together into blocks representing
an entity such as Address, CreditParty, DebitParty, etc. and these groups are
forming the following hierarchical structure:

- onePaymentOrder
    - payment
        - creditParty
            - account
            - address
        - debitParty
            - account
            - address

Each of this entity contains attributes [table].

For each Entity a Generator class is created,
which populates its attributes with random values when initiated.


The output is a JSON with dumped structure.
Outputing can be of 2 modes:
- **Batch** - generates specified number of payments at once,

- **Realtime** - keeps generating (up to a specified limit) payments
    with simulated date/time as it happening in current time.

There are tree modes of date faking:
- **Fully random** - generates fully random date for each transaction,
both submissioDate and dueDate

- **Uniformly distributed** - dates for each transaction will respect an uniform distribution by setting base date and +/- number of days deviation

- **Normally distributed** - dates for each transaction will respect an normal distribution by setting base (mean) date and std. deviation of days

## Configuration File
Configuration file is a YAML file - [config.yml](config.yml)

Sample configuration file with description:
```yml
generator_type: batch  # batch / realtime
number_of_clients: 3  # set 0 to endless when using realtime generator

locale:  # locales and weights of proportional representation
    cs_CZ: 0.8
    en_US: 0.2

currencies: # currencies and weights of proportional representation
    CZK: 0.8
    USD: 0.15
    JPY: 0.05

amount:
    distribution: normal  # normal / uniform
    mean: 2000
    std_deviation: 100

submission_date:  # ignored when realtime
    distribution: normal  # normal / uniform
    mean: 2018-12-14T21:59:43.10-05:00
    std_deviation: 100  # days

```

## Installation
To setup the library (in future will be back as a module),
install dependencies by running `pip install -r requirements.txt`

The library is written for Python 3, but should work in Python 2 as well.


## Usage
Edit the [configuration file](config.yml) and start generating by executing:
```console
python main.py
```
Or, if you wish to redirect the output to a text file:
```console
python main.py > output.json
```

## Sample output
```json
{
    "payment": {
        "transactionType": "WIRE_TRANSFER",
        "amount": 1200,
        "currency": "WST",
        "submissionDate": "2018-04-20.02.03.32",
        "dueDate": "2018-04-20.02.03.32",
        "ks": "2199611211",
        "ss": "2158",
        "urgent": true,
        "vs": "9509296797",
        "additionalInformation": "",
        "creditParty": {
            "name": "Marek Kratochvíl",
            "address": {
                "buildingNumber": "8557",
                "city": "Mimoň",
                "country": "Česká Republika",
                "registryBuildingNumber": "3732",
                "zipCode": "066 16",
                "addressLine1": "Loděnická 8557",
                "addressLine2": "066 16 Mimoň",
                "street": "Loděnická 8557 066 16 Mimoň",
                "cityCombined": "Mimoň 066 16"
            },
            "account": {
                "iban": "GB37EESX8295293514135"
            }
        },
        "debitParty": {
            "name": "Jindřich Svoboda",
            "address": {
                "buildingNumber": "9208",
                "city": "Velvary",
                "country": "Česká Republika",
                "registryBuildingNumber": "6594",
                "zipCode": "130 91",
                "addressLine1": "Štěchovická 9208",
                "addressLine2": "130 91 Velvary",
                "street": "Štěchovická 9208 130 91 Velvary",
                "cityCombined": "Velvary 130 91"
            },
            "account": {
                "iban": "GB47BLXW7851523723109"
            }
        }
    }
}
```
