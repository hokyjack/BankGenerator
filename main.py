from generators import GenerateAddress, GenerateOnePaymentOrder
import json
import yaml
import sys
import datetime

from babel import Locale # TODO: for handling multiple localizations

def handler(obj):
    """Helper handler for converting Object structure into dict-like
    for JSON encoding"""

    if isinstance(obj, datetime.datetime):
        return obj.isoformat()
    else:
        return obj.__dict__

def dump_one_payment_order():
    """Generates and prints 1 transaction on stdout"""
    a = GenerateOnePaymentOrder()
    json.dump(a, sys.stdout, indent=4, ensure_ascii=False, default=handler)

# Open and load YAML settings
config = yaml.safe_load(open("config.yml"))
number_of_clients = config.get("number_of_clients", 1)
generator_type = config.get('generator_type', 'batch')

if generator_type == 'batch':
    for _ in range(number_of_clients):
        dump_one_payment_order()
else:
    cnt = 0
    while True:
        dump_one_payment_order()

        cnt += 1
        if cnt == number_of_clients:
            break
